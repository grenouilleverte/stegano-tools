import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from skimage.filters.rank import entropy
from skimage.morphology import disk

import sys

def main(argv):
    img = Image.open(argv[0])
    img = np.asarray(img, dtype='uint8')

    red = img.copy()[:,:,0]
    blue = img.copy()[:,:,1]
    green = img.copy()[:,:,2]

    def lsb(array, offset=0):
        it = np.nditer(array)
        return np.array([int(i)&2**offset for i in it], dtype='uint8').reshape(array.shape)

    lsbimg = lsb(red, offset=0)

    img_entropy = entropy(lsbimg, disk(10))

    plt.imshow(img_entropy)
    plt.show()

if __name__ == "__main__":
   main(sys.argv[1:])
